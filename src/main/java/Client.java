import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class Client {

    public static void main(String[] args) {
        String ip = args[0];
        int port = Integer.parseInt(args[1]);

        try {
            // Créer une connexion
            Socket socket = new Socket(ip, port);
            System.out.println("DEBUG: Connecté au serveur");

            // Obtenir les flux d'entrée et de sortie
            PrintWriter socketOut = new PrintWriter(socket.getOutputStream(), true);
            BufferedReader socketIn = new BufferedReader(new InputStreamReader(socket.getInputStream()));

            // Lire le message de bienvenue
            String message = socketIn.readLine();
            System.out.println("INFO: " + message);

            // Envoyer le nom
            socketOut.printf("Sebastien\n");

            // Lire la salutation
            System.out.println("INFO: " + socketIn.readLine());

            // Fermer la connexion
            socket.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

}
